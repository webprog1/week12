class CreatedOrderItemDto {
  productId: number;
  amount: number;
}
export class CreateOrderDto {
  customerID: number;
  orderItems: CreatedOrderItemDto[];
}
